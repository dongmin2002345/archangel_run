// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef X_MILKCOW1001_01_ANIM_H__
#define X_MILKCOW1001_01_ANIM_H__

namespace x_Milkcow1001_01_Anim
{
    enum
    {
        IDLE_01                 = 0,
        IDLE_02                 = 1
    };
}

#endif  // #ifndef X_MILKCOW1001_01_ANIM_H__
