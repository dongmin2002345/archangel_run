// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FLAG_PVP_ANIM_H__
#define FLAG_PVP_ANIM_H__

namespace Flag_PVP_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef FLAG_PVP_ANIM_H__
