// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_GUARD_STONE_02_ANIM_H__
#define MISSION_GUARD_STONE_02_ANIM_H__

namespace Mission_Guard_Stone_02_Anim
{
    enum
    {
        DIE_02                  = 1000020,
        DMG_02                  = 1000011,
        IDLE_02                 = 1000000
    };
}

#endif  // #ifndef MISSION_GUARD_STONE_02_ANIM_H__
