// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_GINGERMAN_LEVEL03_ANIM_H__
#define MISSION_GINGERMAN_LEVEL03_ANIM_H__

namespace Mission_gingerman_Level03_Anim
{
    enum
    {
        LEVEL03_DIE_01          = 1000021,
        LEVEL03_DMG_01          = 1000011,
        LEVEL03_IDLE_01         = 1000001
    };
}

#endif  // #ifndef MISSION_GINGERMAN_LEVEL03_ANIM_H__
