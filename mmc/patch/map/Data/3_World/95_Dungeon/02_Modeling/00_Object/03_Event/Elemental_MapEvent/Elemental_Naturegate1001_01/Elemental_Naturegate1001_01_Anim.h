// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELEMENTAL_NATUREGATE1001_01_ANIM_H__
#define ELEMENTAL_NATUREGATE1001_01_ANIM_H__

namespace Elemental_Naturegate1001_01_Anim
{
    enum
    {
        CLOSE_01                = 1000011,
        CLOSEIDLE_01            = 1000000,
        OPEN_01                 = 1000030,
        OPENIDLE_01             = 1000020
    };
}

#endif  // #ifndef ELEMENTAL_NATUREGATE1001_01_ANIM_H__
