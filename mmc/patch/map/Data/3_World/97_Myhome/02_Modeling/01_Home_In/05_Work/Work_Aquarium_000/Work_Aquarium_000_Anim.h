// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WORK_AQUARIUM_000_ANIM_H__
#define WORK_AQUARIUM_000_ANIM_H__

namespace Work_Aquarium_000_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef WORK_AQUARIUM_000_ANIM_H__
