--[[dofile("Script/Effect/Effect_Item.lua")
dofile("Script/Effect/Effect_2501.lua")
dofile("Script/Effect/Effect_2601.lua")
dofile("Script/Effect/Effect_11001.lua")
dofile("Script/Effect/Effect_11201.lua")
dofile("Script/Effect/Effect_12101.lua")
dofile("Script/Effect/Effect_13001.lua")
dofile("Script/Effect/Effect_14001.lua")
dofile("Script/Effect/Effect_6009401.lua")
dofile("Script/Effect/Effect_6013401.lua")
-- 시타
dofile("Script/Effect/Effect_6023901.lua")]]

dofile("Script/Effect/Effect_100011301.lua")
-- Fighter
dofile("Script/Effect/Effect_101000301.lua")
dofile("Script/Effect/Effect_101000401.lua")
dofile("Script/Effect/Effect_101000601.lua")
dofile("Script/Effect/Effect_101000701.lua")
dofile("Script/Effect/Effect_101000801.lua")
dofile("Script/Effect/Effect_101300101.lua")
-- Magician
dofile("Script/Effect/Effect_102000201.lua")
dofile("Script/Effect/Effect_102000301.lua")
dofile("Script/Effect/Effect_102000401.lua")
dofile("Script/Effect/Effect_102000601.lua")
dofile("Script/Effect/Effect_102000701.lua")
dofile("Script/Effect/Effect_102000801.lua")
dofile("Script/Effect/Effect_102001001.lua")
dofile("Script/Effect/Effect_102101201.lua")
dofile("Script/Effect/Effect_102002101.lua")
dofile("Script/Effect/Effect_102300101.lua")
-- Archer
dofile("Script/Effect/Effect_103000301.lua")
dofile("Script/Effect/Effect_103000401.lua")
dofile("Script/Effect/Effect_103000501.lua")
dofile("Script/Effect/Effect_103000701.lua")
dofile("Script/Effect/Effect_103000801.lua")
dofile("Script/Effect/Effect_103300101.lua")
-- Theif
dofile("Script/Effect/Effect_104000101.lua")
dofile("Script/Effect/Effect_104000201.lua")
dofile("Script/Effect/Effect_104000301.lua")
dofile("Script/Effect/Effect_104000601.lua")
dofile("Script/Effect/Effect_104000701.lua")
dofile("Script/Effect/Effect_104000801.lua")
dofile("Script/Effect/Effect_104300101.lua")
dofile("Script/Effect/Effect_104300201.lua")
dofile("Script/Effect/Effect_104300301.lua")
dofile("Script/Effect/Effect_104300701.lua")
dofile("Script/Effect/Effect_104301201.lua")
dofile("Script/Effect/Effect_104301301.lua")
dofile("Script/Effect/Effect_104301401.lua")
dofile("Script/Effect/Effect_104301501.lua")
-- Knight
dofile("Script/Effect/Effect_105000201.lua")
dofile("Script/Effect/Effect_105000301.lua")
dofile("Script/Effect/Effect_105020201.lua")
dofile("Script/Effect/Effect_105300101.lua")
dofile("Script/Effect/Effect_105300201.lua")
dofile("Script/Effect/Effect_105300301.lua")
dofile("Script/Effect/Effect_105300501.lua")
dofile("Script/Effect/Effect_105300601.lua")
dofile("Script/Effect/Effect_105300701.lua")
dofile("Script/Effect/Effect_105300801.lua")
dofile("Script/Effect/Effect_105300901.lua")
dofile("Script/Effect/Effect_105400001.lua")
dofile("Script/Effect/Effect_105500501.lua")

-- 투사
dofile("Script/Effect/Effect_106000201.lua")
dofile("Script/Effect/Effect_106020201.lua")
dofile("Script/Effect/Effect_106300101.lua")
dofile("Script/Effect/Effect_106300201.lua")
dofile("Script/Effect/Effect_106300401.lua")
dofile("Script/Effect/Effect_106300501.lua")
dofile("Script/Effect/Effect_106300601.lua")
dofile("Script/Effect/Effect_106300701.lua")
dofile("Script/Effect/Effect_106400201.lua")
dofile("Script/Effect/Effect_106400401.lua")
dofile("Script/Effect/Effect_106500001.lua")
dofile("Script/Effect/Effect_106500101.lua")

dofile("Script/Effect/Effect_107000701.lua")

dofile("Script/Effect/Effect_108000101.lua")
dofile("Script/Effect/Effect_1080001001.lua")
dofile("Script/Effect/Effect_109000201.lua")
dofile("Script/Effect/Effect_109000301.lua")
dofile("Script/Effect/Effect_109000401.lua")
dofile("Script/Effect/Effect_1090004001.lua")
dofile("Script/Effect/Effect_109000901.lua")

dofile("Script/Effect/Effect_110000401.lua")
dofile("Script/Effect/Effect_110000501.lua")
dofile("Script/Effect/Effect_110000601.lua")
dofile("Script/Effect/Effect_110000801.lua")
dofile("Script/Effect/Effect_110000901.lua")
dofile("Script/Effect/Effect_110001001.lua")
dofile("Script/Effect/Effect_110001401.lua")
dofile("Script/Effect/Effect_110001501.lua")
dofile("Script/Effect/Effect_110001701.lua")
dofile("Script/Effect/Effect_110001801.lua")
dofile("Script/Effect/Effect_110002001.lua")

--성기사
dofile("Script/Effect/Effect_105500801.lua") -- 바리케이드
dofile("Script/Effect/Effect_105500901.lua") -- 리커버리 오라 / 리커버링 오라<효과>
dofile("Script/Effect/Effect_105501001.lua") -- 타임리버스 오라 / 타임리버스 오라<효과>
dofile("Script/Effect/Effect_105501101.lua") -- 오펜시브 오라 / 오펜시브 오라<효과>
dofile("Script/Effect/Effect_105501201.lua") -- 디펜시브 오라 / 디펜시브 오라<효과>
dofile("Script/Effect/Effect_105501301.lua") -- 성스러운 보호 / 성스러운 보호<효과>

--검투사
dofile("Script/Effect/Effect_106500801.lua") -- 웨폰 퀴큰

--위자드
dofile("Script/Effect/Effect_109001101.lua") -- MP 포션 극대화
dofile("Script/Effect/Effect_109001301.lua") -- 스트라이킹
dofile("Script/Effect/Effect_109001401.lua") -- 썬더브레이크(예전)
dofile("Script/Effect/Effect_109001501.lua") -- 체인라이트닝
dofile("Script/Effect/Effect_109001601.lua") -- HP 리스토어
dofile("Script/Effect/Effect_109001701.lua") -- MP 전이
dofile("Script/Effect/Effect_109001801.lua") -- 배리어
dofile("Script/Effect/Effect_109001901.lua") -- 완벽한 결계

--워매이지
dofile("Script/Effect/Effect_109001201.lua")
dofile("Script/Effect/Effect_109002001.lua") -- 미러이미지
--스나이퍼
dofile("Script/Effect/Effect_150000401.lua")
-- 트랩퍼
--dofile("Script/Effect/Effect_140000001.lua")
--dofile("Script/Effect/Effect_140000101.lua")
--dofile("Script/Effect/Effect_140000201.lua")
dofile("Script/Effect/Effect_110002301.lua") -- 집중
dofile("Script/Effect/Effect_110002501.lua") -- 건들지마라!
dofile("Script/Effect/Effect_110002601.lua") -- 프리징 트랩
dofile("Script/Effect/Effect_110002701.lua") -- 사일런스 트랩
dofile("Script/Effect/Effect_110002801.lua") -- 엠피제로 트랩
dofile("Script/Effect/Effect_110002901.lua") -- 스모크 그레네이드

-- 저격수
dofile("Script/Effect/Effect_150000601.lua") -- 플래시뱅
dofile("Script/Effect/Effect_150000701.lua") -- 아드레날린
dofile("Script/Effect/Effect_150000801.lua") -- WP 그레네이드
dofile("Script/Effect/Effect_150000901.lua") -- 아트로핀

-- Dancer
dofile("Script/Effect/Effect_160000001.lua")
dofile("Script/Effect/Effect_160000101.lua")
dofile("Script/Effect/Effect_160000501.lua")

--닌자
dofile("Script/Effect/Effect_170000201.lua")
dofile("Script/Effect/Effect_170000301.lua")
dofile("Script/Effect/Effect_1700005011.lua") -- 목둔술! 통나무 굴리기 발동
dofile("Script/Effect/Effect_170000601.lua") -- 환영분신
dofile("Script/Effect/Effect_170000701.lua") -- 격!일섬!

-- 공통 : 상태이상
dofile("Script/Effect/Effect_100010001.lua")
dofile("Script/Effect/Effect_100010101.lua")
dofile("Script/Effect/Effect_100010201.lua")
dofile("Script/Effect/Effect_100010301.lua")
dofile("Script/Effect/Effect_100010401.lua")
dofile("Script/Effect/Effect_100010501.lua")
dofile("Script/Effect/Effect_100010601.lua")
dofile("Script/Effect/Effect_100010701.lua")
dofile("Script/Effect/Effect_100010801.lua")
dofile("Script/Effect/Effect_100010901.lua")
dofile("Script/Effect/Effect_100011001.lua")
dofile("Script/Effect/Effect_100011101.lua")
dofile("Script/Effect/Effect_100011201.lua")
dofile("Script/Effect/Effect_101020101.lua")

-- Boss
-- Lavalon
--[[dofile("Script/Effect/Effect_6000900.lua")
dofile("Script/Effect/Effect_6000901.lua")
dofile("Script/Effect/Effect_6000902.lua")
dofile("Script/Effect/Effect_6000903.lua")
dofile("Script/Effect/Effect_6000904.lua")
-- Lavalon heart
dofile("Script/Effect/Effect_6033201.lua")
dofile("Script/Effect/Effect_6033202.lua")]]

--GUILD
dofile("Script/Effect/Effect_200000001.lua")
dofile("Script/Effect/Effect_200000101.lua")
dofile("Script/Effect/Effect_200000201.lua")
dofile("Script/Effect/Effect_200000301.lua")

--Couple
dofile("Script/Effect/Effect_90000001.lua")
