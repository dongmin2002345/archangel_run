dofile("Script/Skill/Skill_const.lua")
dofile("Script/Skill/Skill_CS_Common1.lua")
dofile("Script/Skill/Skill_default.lua")
dofile("Script/Skill/Action_100000101.lua")
----------------------------------------
-- 보쓰(Lavalon)
dofile("Script/Skill/Skill_900000001.lua")
dofile("Script/Skill/Skill_900000011.lua")
dofile("Script/Skill/Skill_900000021.lua")
dofile("Script/Skill/Skill_900000031.lua")
dofile("Script/Skill/Skill_141.lua")

-- 몬스터
--[[dofile("Script/Skill/Skill_1.lua")
--dofile("Script/Skill/Skill_10.lua")
--dofile("Script/Skill/Skill_20.lua")
--dofile("Script/Skill/Skill_100.lua")
dofile("Script/Skill/Skill_10101.lua")
dofile("Script/Skill/Skill_6053100.lua")	--자폭 스킬
dofile("Script/Skill/Skill_142.lua")
dofile("Script/Skill/Skill_143.lua")

-- lavalon
dofile("Script/Skill/Skill_6000900.lua")
dofile("Script/Skill/Skill_6000901.lua")
dofile("Script/Skill/Skill_6000902.lua")
dofile("Script/Skill/Skill_6000903.lua")

-- lavalon heart
dofile("Script/Skill/Skill_6033201.lua")
dofile("Script/Skill/Skill_6033202.lua")

dofile("Script/Skill/Skill_6003101.lua")
dofile("Script/Skill/Skill_6004904.lua")
dofile("Script/Skill/Skill_6006902.lua")
dofile("Script/Skill/Skill_6006903.lua")
dofile("Script/Skill/Skill_6009602.lua")
dofile("Script/Skill/Skill_6009603.lua")
dofile("Script/Skill/Skill_6009604.lua")
dofile("Script/Skill/Skill_6013903.lua")
dofile("Script/Skill/Skill_6013904.lua")
dofile("Script/Skill/Skill_6013905.lua")
dofile("Script/Skill/Skill_6013909.lua")
dofile("Script/Skill/Skill_6015903.lua")
dofile("Script/Skill/Skill_6025904.lua")
dofile("Script/Skill/Skill_6018904.lua")
dofile("Script/Skill/Skill_60189041.lua")
dofile("Script/Skill/Skill_6004201.lua")]]
-- 전사 스킬
dofile("Script/Skill/Skill_101000101.lua")
dofile("Script/Skill/Skill_101000201.lua")
dofile("Script/Skill/Skill_101000301.lua")
dofile("Script/Skill/Skill_101000401.lua")
dofile("Script/Skill/Skill_101000501.lua")
dofile("Script/Skill/Skill_101000601.lua")
dofile("Script/Skill/Skill_101000701.lua")
dofile("Script/Skill/Skill_101000801.lua")
dofile("Script/Skill/Skill_101000901.lua")
dofile("Script/Skill/Skill_101001001.lua")
dofile("Script/Skill/Skill_101100101.lua")
dofile("Script/Skill/Skill_101100201.lua")
dofile("Script/Skill/Skill_101100301.lua")
dofile("Script/Skill/Skill_101100401.lua")
dofile("Script/Skill/Skill_101100501.lua")
dofile("Script/Skill/Skill_101100601.lua")
dofile("Script/Skill/Skill_101100701.lua")
dofile("Script/Skill/Skill_101100801.lua")
dofile("Script/Skill/Skill_101100901.lua")
dofile("Script/Skill/Skill_101101001.lua")
dofile("Script/Skill/Skill_101101101.lua")
dofile("Script/Skill/Skill_101101201.lua")
dofile("Script/Skill/Skill_101101301.lua")
dofile("Script/Skill/Skill_101101401.lua")
dofile("Script/Skill/Skill_101101501.lua")
dofile("Script/Skill/Skill_101101601.lua")
dofile("Script/Skill/Skill_101101701.lua")
dofile("Script/Skill/Skill_101101801.lua")
dofile("Script/Skill/Skill_101101901.lua")
dofile("Script/Skill/Skill_101102001.lua")
dofile("Script/Skill/Skill_101102101.lua")
dofile("Script/Skill/Skill_101102201.lua")
dofile("Script/Skill/Skill_101102301.lua")
dofile("Script/Skill/Skill_101102401.lua")
dofile("Script/Skill/Skill_101102501.lua")
dofile("Script/Skill/Skill_101102601.lua")
dofile("Script/Skill/Skill_101102701.lua")
dofile("Script/Skill/Skill_101102801.lua")
dofile("Script/Skill/Skill_101102901.lua")
dofile("Script/Skill/Skill_101103001.lua")
dofile("Script/Skill/Skill_101300101.lua")
-- 궁수
dofile("Script/Skill/Skill_102000101.lua")
dofile("Script/Skill/Skill_102000201.lua")
dofile("Script/Skill/Skill_102000301.lua")
dofile("Script/Skill/Skill_102000401.lua")
dofile("Script/Skill/Skill_102000501.lua")
dofile("Script/Skill/Skill_102000601.lua")
dofile("Script/Skill/Skill_102000701.lua")
dofile("Script/Skill/Skill_102000801.lua")
dofile("Script/Skill/Skill_102001001.lua")
dofile("Script/Skill/Skill_102001101.lua")
dofile("Script/Skill/Skill_102002001.lua")
dofile("Script/Skill/Skill_102002101.lua")
dofile("Script/Skill/Skill_102001101.lua")
dofile("Script/Skill/Skill_102100101.lua")
dofile("Script/Skill/Skill_102100201.lua")
dofile("Script/Skill/Skill_102100301.lua")
dofile("Script/Skill/Skill_102100401.lua")
dofile("Script/Skill/Skill_102100501.lua")
dofile("Script/Skill/Skill_102100601.lua")
dofile("Script/Skill/Skill_102100701.lua")
dofile("Script/Skill/Skill_102100801.lua")
dofile("Script/Skill/Skill_102100901.lua")
dofile("Script/Skill/Skill_102101301.lua")
dofile("Script/Skill/Skill_102101401.lua")
dofile("Script/Skill/Skill_102101501.lua")
dofile("Script/Skill/Skill_102101601.lua")
dofile("Script/Skill/Skill_102101701.lua")
dofile("Script/Skill/Skill_102101801.lua")
dofile("Script/Skill/Skill_102101001.lua")
dofile("Script/Skill/Skill_102101201.lua")
dofile("Script/Skill/Skill_102102001.lua")
dofile("Script/Skill/Skill_102102101.lua")
dofile("Script/Skill/Skill_102102201.lua")
dofile("Script/Skill/Skill_102102301.lua")
dofile("Script/Skill/Skill_102102401.lua")
dofile("Script/Skill/Skill_102102501.lua")
dofile("Script/Skill/Skill_102102601.lua")
dofile("Script/Skill/Skill_102102701.lua")
dofile("Script/Skill/Skill_102102801.lua")
dofile("Script/Skill/Skill_102300101.lua")
-- 마법사
dofile("Script/Skill/Skill_103000101.lua")
dofile("Script/Skill/Skill_103000201.lua")
dofile("Script/Skill/Skill_103000301.lua")
dofile("Script/Skill/Skill_103000401.lua")
dofile("Script/Skill/Skill_103000501.lua")
dofile("Script/Skill/Skill_103000601.lua")
dofile("Script/Skill/Skill_103000701.lua")
dofile("Script/Skill/Skill_103000801.lua")
dofile("Script/Skill/Skill_103000901.lua")
dofile("Script/Skill/Skill_103100101.lua")
dofile("Script/Skill/Skill_103100201.lua")
dofile("Script/Skill/Skill_103100301.lua")
dofile("Script/Skill/Skill_103100401.lua")
dofile("Script/Skill/Skill_103100501.lua")
dofile("Script/Skill/Skill_103100601.lua")
dofile("Script/Skill/Skill_103100701.lua")
dofile("Script/Skill/Skill_103100801.lua")
dofile("Script/Skill/Skill_103100901.lua")
dofile("Script/Skill/Skill_103101001.lua")
dofile("Script/Skill/Skill_103101101.lua")
dofile("Script/Skill/Skill_103101201.lua")
dofile("Script/Skill/Skill_103101301.lua")
dofile("Script/Skill/Skill_103101301.lua")
dofile("Script/Skill/Skill_103101401.lua")
dofile("Script/Skill/Skill_103101501.lua")
dofile("Script/Skill/Skill_103101601.lua")
dofile("Script/Skill/Skill_103101701.lua")
dofile("Script/Skill/Skill_103101801.lua")
dofile("Script/Skill/Skill_103101901.lua")
dofile("Script/Skill/Skill_103200101.lua")
dofile("Script/Skill/Skill_103200101.lua")
dofile("Script/Skill/Skill_103200201.lua")
dofile("Script/Skill/Skill_103200301.lua")
dofile("Script/Skill/Skill_103200401.lua")
dofile("Script/Skill/Skill_103200501.lua")
dofile("Script/Skill/Skill_103200601.lua")
dofile("Script/Skill/Skill_103200701.lua")
dofile("Script/Skill/Skill_103200801.lua")
dofile("Script/Skill/Skill_103200901.lua")
dofile("Script/Skill/Skill_103201001.lua")
dofile("Script/Skill/Skill_103201101.lua")
dofile("Script/Skill/Skill_103201201.lua")
dofile("Script/Skill/Skill_103201301.lua")
dofile("Script/Skill/Skill_103201401.lua")
dofile("Script/Skill/Skill_103201501.lua")
dofile("Script/Skill/Skill_103201601.lua")
dofile("Script/Skill/Skill_103201701.lua")
dofile("Script/Skill/Skill_103201801.lua")
dofile("Script/Skill/Skill_103201901.lua")
dofile("Script/Skill/Skill_103202001.lua")
dofile("Script/Skill/Skill_103202101.lua")
dofile("Script/Skill/Skill_103202201.lua")
dofile("Script/Skill/Skill_103202301.lua")
dofile("Script/Skill/Skill_103300101.lua")
-- 도적
dofile("Script/Skill/Skill_104000101.lua")
dofile("Script/Skill/Skill_104000201.lua")
dofile("Script/Skill/Skill_104000301.lua")
dofile("Script/Skill/Skill_104000401.lua")
dofile("Script/Skill/Skill_104000501.lua")
dofile("Script/Skill/Skill_104000601.lua")
dofile("Script/Skill/Skill_104000701.lua")
dofile("Script/Skill/Skill_104000801.lua")
dofile("Script/Skill/Skill_104100101.lua")
dofile("Script/Skill/Skill_104100201.lua")
dofile("Script/Skill/Skill_104300101.lua")
dofile("Script/Skill/Skill_104300201.lua")
dofile("Script/Skill/Skill_104300301.lua")
dofile("Script/Skill/Skill_104300401.lua")
dofile("Script/Skill/Skill_104300501.lua")
dofile("Script/Skill/Skill_104300601.lua")
dofile("Script/Skill/Skill_104300701.lua")
dofile("Script/Skill/Skill_104300801.lua")
dofile("Script/Skill/Skill_104300901.lua")
dofile("Script/Skill/Skill_104301001.lua")
dofile("Script/Skill/Skill_104301101.lua")
dofile("Script/Skill/Skill_104301201.lua")
dofile("Script/Skill/Skill_104301301.lua")
dofile("Script/Skill/Skill_104301401.lua")
dofile("Script/Skill/Skill_104301501.lua")
dofile("Script/Skill/Skill_104301601.lua")
dofile("Script/Skill/Skill_104301701.lua")
dofile("Script/Skill/Skill_104301801.lua")
dofile("Script/Skill/Skill_103202701.lua")
dofile("Script/Skill/Skill_103202501.lua")
dofile("Script/Skill/Skill_103202601.lua")
dofile("Script/Skill/Skill_103202401.lua")


dofile("Script/Skill/Skill_105000101.lua")
dofile("Script/Skill/Skill_105000201.lua")
dofile("Script/Skill/Skill_105000301.lua")

dofile("Script/Skill/Skill_105300201.lua")
dofile("Script/Skill/Skill_105300301.lua")
dofile("Script/Skill/Skill_105300401.lua")
dofile("Script/Skill/Skill_105300501.lua")
dofile("Script/Skill/Skill_105300601.lua")
dofile("Script/Skill/Skill_105300701.lua")
dofile("Script/Skill/Skill_105300801.lua")
dofile("Script/Skill/Skill_105300901.lua")

dofile("Script/Skill/Skill_106000101.lua")
dofile("Script/Skill/Skill_106000201.lua")
dofile("Script/Skill/Skill_106300101.lua")
dofile("Script/Skill/Skill_106300201.lua")
dofile("Script/Skill/Skill_106300301.lua")
dofile("Script/Skill/Skill_106300401.lua")
dofile("Script/Skill/Skill_106300501.lua")
dofile("Script/Skill/Skill_106300601.lua")
dofile("Script/Skill/Skill_106300701.lua")
dofile("Script/Skill/Skill_106400001.lua")
dofile("Script/Skill/Skill_106400101.lua")
dofile("Script/Skill/Skill_106400201.lua")
dofile("Script/Skill/Skill_106400401.lua")
dofile("Script/Skill/Skill_106500001.lua")
dofile("Script/Skill/Skill_106500101.lua")
dofile("Script/Skill/Skill_106500201.lua")
dofile("Script/Skill/Skill_106500401.lua")

dofile("Script/Skill/Skill_107000101.lua")
dofile("Script/Skill/Skill_107000201.lua")
dofile("Script/Skill/Skill_107000301.lua")
dofile("Script/Skill/Skill_107000401.lua")
dofile("Script/Skill/Skill_107000501.lua")
dofile("Script/Skill/Skill_107000601.lua")
dofile("Script/Skill/Skill_107000701.lua")
--전투마법사
dofile("Script/Skill/Skill_108000101.lua")
dofile("Script/Skill/Skill_1080001011.lua")
dofile("Script/Skill/Skill_108000201.lua")
-- 사냥꾼
dofile("Script/Skill/Skill_109000101.lua")
dofile("Script/Skill/Skill_109000201.lua")
dofile("Script/Skill/Skill_109000301.lua")
dofile("Script/Skill/Skill_109000401.lua")
dofile("Script/Skill/Skill_1090004011.lua")
dofile("Script/Skill/Skill_109000501.lua")
dofile("Script/Skill/Skill_109000601.lua")
dofile("Script/Skill/Skill_109000701.lua")
dofile("Script/Skill/Skill_109000801.lua")
dofile("Script/Skill/Skill_109000901.lua")
dofile("Script/Skill/Skill_1090009011.lua")
dofile("Script/Skill/Skill_109001001.lua")
--레인저
dofile("Script/Skill/Skill_110000101.lua")
dofile("Script/Skill/Skill_110000201.lua")
dofile("Script/Skill/Skill_110000301.lua")
dofile("Script/Skill/Skill_110000401.lua")
dofile("Script/Skill/Skill_110000501.lua")
dofile("Script/Skill/Skill_110000601.lua")
dofile("Script/Skill/Skill_110000701.lua")
dofile("Script/Skill/Skill_110000801.lua")
dofile("Script/Skill/Skill_1100008011.lua")
dofile("Script/Skill/Skill_110000901.lua")
dofile("Script/Skill/Skill_110001001.lua")
dofile("Script/Skill/Skill_110001101.lua")
dofile("Script/Skill/Skill_110001201.lua")
dofile("Script/Skill/Skill_110001301.lua")
dofile("Script/Skill/Skill_110001401.lua")
dofile("Script/Skill/Skill_1100014011.lua")
dofile("Script/Skill/Skill_110001501.lua")
dofile("Script/Skill/Skill_1100015011.lua")
dofile("Script/Skill/Skill_110001601.lua")
dofile("Script/Skill/Skill_110001701.lua")
dofile("Script/Skill/Skill_110001801.lua")
dofile("Script/Skill/Skill_110001901.lua")
dofile("Script/Skill/Skill_110002001.lua")
dofile("Script/Skill/Skill_110002101.lua")
dofile("Script/Skill/Skill_110002201.lua")
dofile("Script/Skill/Skill_110002401.lua")
dofile("Script/Skill/Skill_150000301.lua") -- 바주카포

--성기사
dofile("Script/Skill/Skill_105300101.lua") -- 아머 마스터리
dofile("Script/Skill/Skill_105400001.lua") -- 바디 액티베이션
dofile("Script/Skill/Skill_105500501.lua") -- 오라 발현
dofile("Script/Skill/Skill_105500601.lua") -- 크로스 컷
dofile("Script/Skill/Skill_105500701.lua") -- 프레셔
dofile("Script/Skill/Skill_105500801.lua") -- 바리케이드
dofile("Script/Skill/Skill_105500901.lua") -- 리커버리 오라
dofile("Script/Skill/Skill_105501001.lua") -- 타임 리버스
dofile("Script/Skill/Skill_105501101.lua") -- 오펜시브 오라
dofile("Script/Skill/Skill_105501201.lua") -- 디펜시브 오라
dofile("Script/Skill/Skill_105501301.lua") -- 성스러운 보호
dofile("Script/Skill/Skill_105501401.lua") -- 장비 수리

--검투사
dofile("Script/Skill/Skill_106500501.lua") -- 거스트 슬래시
dofile("Script/Skill/Skill_106500601.lua") -- 매그넘 브레이크
dofile("Script/Skill/Skill_106500701.lua") -- 데스 바운드
dofile("Script/Skill/Skill_106500801.lua") -- 웨폰 퀴큰
dofile("Script/Skill/Skill_106500901.lua") -- 웨폰 브레이크
dofile("Script/Skill/Skill_1065009011.lua") -- 웨폰 브레이크 발사

-- 위자드
dofile("Script/Skill/Skill_109001101.lua") -- MP포션 극대화
dofile("Script/Skill/Skill_109001301.lua") -- 스트라이킹
dofile("Script/Skill/Skill_109001401.lua") -- 썬더 브레이크
dofile("Script/Skill/Skill_109001501.lua") -- 체인라이트닝
dofile("Script/Skill/Skill_109001601.lua") -- HP 리스토어
dofile("Script/Skill/Skill_109001701.lua") -- MP 전이
dofile("Script/Skill/Skill_109001801.lua") -- 배리어
dofile("Script/Skill/Skill_109001901.lua") -- 완벽한 결계
dofile("Script/Skill/Skill_109002701.lua") -- 장비 인챈트

-- 위메이지
dofile("Script/Skill/Skill_109001201.lua") -- 어드벤스드 스피어 마스터리
dofile("Script/Skill/Skill_109002001.lua") -- 미러이미지
dofile("Script/Skill/Skill_109002101.lua") -- 리소스 컨버터
dofile("Script/Skill/Skill_109002201.lua") -- 먹물발사
dofile("Script/Skill/Skill_109002301.lua") -- 스노으 블루스
dofile("Script/Skill/Skill_1090023011.lua")
dofile("Script/Skill/Skill_109002401.lua") -- 백 드래프트
dofile("Script/Skill/Skill_1090024011.lua") -- 백 드래프트 발사
dofile("Script/Skill/Skill_109002501.lua") -- 클라우드 킬
dofile("Script/Skill/Skill_1090025011.lua") -- 클라우드 킬 발사
dofile("Script/Skill/Skill_109002601.lua") -- 메테오스트라이크

-- 트랩퍼
dofile("Script/Skill/Skill_140000001.lua") -- 컨센트레이트(예전)
dofile("Script/Skill/Skill_140000101.lua") -- 관통(예전)
dofile("Script/Skill/Skill_140000201.lua") -- 건들지마(예전)
dofile("Script/Skill/Skill_140000301.lua") -- 알 수 없는 요리(예전)
--dofile("Script/Skill/Skill_140000401.lua") -- 사일러스 트랩(예전)
--dofile("Script/Skill/Skill_1400004011.lua")
dofile("Script/Skill/Skill_140000501.lua") -- 마나 번(예전)
dofile("Script/Skill/Skill_1400005011.lua")
dofile("Script/Skill/Skill_110002301.lua") -- 집중
dofile("Script/Skill/Skill_110002501.lua") -- 건들지마라
dofile("Script/Skill/Skill_110002601.lua") -- 프리징 트랩
dofile("Script/Skill/Skill_1100026011.lua") -- 프리징 트랩 발동
dofile("Script/Skill/Skill_110002701.lua") -- 사일런스 트랩
dofile("Script/Skill/Skill_1100027011.lua") -- 프리징 트랩 발동
dofile("Script/Skill/Skill_110002801.lua") -- MP제로 트랩
dofile("Script/Skill/Skill_1100028011.lua") -- MP제로 트랩 발동
dofile("Script/Skill/Skill_110002901.lua") -- 스모크 그레네이드
dofile("Script/Skill/Skill_1100029011.lua") -- 스모크 그레네이드 설치
dofile("Script/Skill/Skill_110003001.lua") -- HE 그레네이드
dofile("Script/Skill/Skill_110003101.lua") -- 요리 하기

-- Sniper
dofile("Script/Skill/Skill_150000401.lua") -- 관통
dofile("Script/Skill/Skill_150000001.lua")
dofile("Script/Skill/Skill_150000101.lua")
dofile("Script/Skill/Skill_150000201.lua")
dofile("Script/Skill/Skill_150000501.lua") -- 핀포인트 어택
dofile("Script/Skill/Skill_150000701.lua") -- 아드레날린
dofile("Script/Skill/Skill_150000801.lua") -- WP 그레네이드
dofile("Script/Skill/Skill_1500008011.lua") -- WP 그레네이드 설치
dofile("Script/Skill/Skill_150000901.lua") -- 아트로핀
dofile("Script/Skill/Skill_150001101.lua") -- 융단폭격
dofile("Script/Skill/Skill_1500011011.lua") -- 융단폭격 발사
dofile("Script/Skill/Skill_150000601.lua") -- 플래시뱅
dofile("Script/Skill/Skill_150001001.lua") -- 개틀링 러쉬

-- Dancer
dofile("Script/Skill/Skill_160000001.lua") -- 스탠스
dofile("Script/Skill/Skill_160000101.lua") -- 헤드스핀
dofile("Script/Skill/Skill_160000201.lua") -- 윈드밀
dofile("Script/Skill/Skill_160000301.lua") -- 세븐
dofile("Script/Skill/Skill_160000401.lua") -- 문워크
dofile("Script/Skill/Skill_160000501.lua") -- 기합
dofile("Script/Skill/Skill_160000601.lua") -- 에어리얼 킥

-- Ninja
dofile("Script/Skill/Skill_170000001.lua")
dofile("Script/Skill/Skill_170000101.lua")

dofile("Script/Skill/Skill_170000201.lua") -- 허공밟기 
dofile("Script/Skill/Skill_170000301.lua") -- 미혼향
dofile("Script/Skill/Skill_170000401.lua") -- 압정
dofile("Script/Skill/Skill_1700004011.lua") -- 압정 설치
dofile("Script/Skill/Skill_170000501.lua") -- 목둔술 통나무 굴리기
dofile("Script/Skill/Skill_1700005011.lua") -- 목둔술 통나무 굴리기 발동
dofile("Script/Skill/Skill_170000601.lua") -- 환영분신
dofile("Script/Skill/Skill_170000701.lua") -- 격!일섬!

--Guild
dofile("Script/Skill/Skill_200000001.lua")
dofile("Script/Skill/Skill_200000101.lua")
dofile("Script/Skill/Skill_200000201.lua")
dofile("Script/Skill/Skill_200000301.lua")

--Couple
dofile("Script/Skill/Skill_90000001.lua")
