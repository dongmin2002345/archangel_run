-- 길드 - 아드레날린 부스터(액셀레이션)
function Skill_Begin200000301(caster, skillnum, iStatus,arg)
	return Skill_Begin106300201(caster, skillnum, iStatus,arg)
end

function Skill_Fire200000301(caster, kTargetArray, skillnum, result, arg)
	return Skill_Fire106300201(caster, kTargetArray, skillnum, result, arg)
end

function Skill_Fail200000301(caster, kTargetArray, skillnum, result, arg)
	return Skill_Fail106300201(caster, kTargetArray, skillnum, result, arg)
end
